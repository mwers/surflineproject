# Surfline Project

## Prerequisites

* Terraform installed
* Google Cloud Platform (GCP) Account
  * GCP service account with the editor role for the project.
* Gitlab Account

## Overview

This project uses GCP and Google Kubernetes Engine (GKE) to run a JavaScript web app that is deployed using Gitlab's CI/CD.

## Instructions Summary

This is the TL;DR. Below are the instructions if you want to deploy mine or your own Terraform configuration and app to GKE.

* Download the GCP service account json file.
* Create Terraform files.
* Apply the Terraform configuration to GCP.
* Create your JavaScript code and package.json file.
* Create your Dockerfile.
* Create Kubernetes deployment.yml and services.yml.
* Add GKE service account secret and registry secret to Gitlab pipeline
  * In your project under Settings->CI/CD->Secret variables
* Create your .gitlab-ci.yml file.
* Do a git commit and push to gitlab.
* Gitlab will automatically deploy your app to the Kubernetes cluster specified.

## Creating Container Engine

I choose to use GKE, which runs within GCP. The Kubernetes cluster was deployed via Terrafrom.

* Download the GCP service account json file.
* Create your Terraform files.
  * [surfline-gke.tf](sufline-gke.tf)
  * [variables.tf](variables.tf)
  * terraform.tfvars (Do not upload to the remote git repo)
* In the same directory as the Terraform files run the following command:

  ```bash
  terraform apply
  ```

* Verify the Kubernetes cluster was created. You can do this via the GCP web console or gcloud cli with kubectl.

## Creating Gitlab CI/CD Pipeline

The files in this section are used by Gitlab to deploy your app to GKE. Create the .gitlab-ci.yml file last, once it is created every push to Gitlab will run the pipeline.

### JavaScript App

Create your JavaScript app and package.json file. These will be inserted into the Docker image that will be used by GKE.

**Files:**

* [myapp.js](myapp.js)
* [package.json](package.json)

### Dockerfile

This will be used by Gitlab to package your JavaScript App into a Node.js container and deployed to your GKE cluster.

**File:**

* [Dockerfile](Dockerfile)

### Kubernetes Deployment and Services Files

The deployment file will deploy and configure your pods as specified across your cluster. The services file will create the load balancer type and expose the app to the internet. These files could be combined into one file, but I have left them separate.

**Files:**

* [deployment.yml](deployment.yml)
* [services.yml](services.yml)

### Create Pipeline Secrets

Two secrets will be used during the pipeline deploy. To configure the secrets, in your Gitlab project navigate to Settings->CI/CD->Secret variables

* The Google GCP service account used to deploy the images to the container.
* The Gitlab secret used to pull the container image from the Gitlab private image repo.

![alt text](GitlabSecrets.png)

### Gitlab CI File

The .gitlab-ci.yml uses the above files to package and deploy your app to GKE. Everytime you push a commit to Gitlab it will run the pipeline.

**File:**

* [.gitlab-ci.yml](.gitlab-ci.yml)