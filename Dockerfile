FROM node:latest
# Create app directory
WORKDIR /usr/src/myapp
# Install app dependencies
COPY package.json ./
RUN npm install
COPY myapp.js ./
EXPOSE 3000
CMD ["npm", "start"]